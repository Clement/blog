Le Jamman Solo est un pédale indispensable. Elle permet de travailler à la fois la créativité et le rythme. Il est possible d'y ajouter une pédale complémentaire: la fs3x . Cette pédale permet de changer de boucle et surtout de choisir le tempo en tap-tempo et d'arrêter la lecture/enregistrement de la boucle. Cette pédale peut se trouver dans les environs de 50€ mais j'ai décider d'en fabriquer une.Quelques modèles ont déjà été fait, notamment celui de JibHaine qui à la particularité d'utiliser une boite de maquereaux en guise de coffret.

![Réalisation][id1]

Montage du circuit électronique

Le schéma vient du blog de Jibaine. Une autre version est disponible sur diyguitarist.

Schéma electrique

    D1, D2: diodes basiques (1N400)
    S1, S2, S3: interrupteurs
    Jack: jack femelle stéréo

J'ai monté les composants sur une plaque à essai. Une fois le schéma validé, j'ai soudé sur une plaque à trous pastillé. De cette manière, le montage peut être montée ou démonter rapidement. Ensuite vient la partie la plus compliquée: le coffret.

![Réalisation][id3]

Réalisation du coffret

A partir d'un tube rectangulaire, j'ai enlever le fond pour insérer facilement les boutons poussoirs

Ensuite on perce les trous dans lesquels on va loger les boutons poussoirs et l'arrivée du jack stéréo. 

Pour la finition, j'ai resoudé (soudure à l'arc) une plaque à chaque extrémité de telle sorte que seul le fond du coffret est ouvert. Ensuite j'ai peint en noir, et pour un rendu encore plus classe, j'ai réalisé un pochoir pour la face avant à l'aide de Inkscape et d'un plotter de découpe. Cette méthode permet même de peindre du texte sur le coffret.

Bilan

L'électronique est bien pensé et sa réalisation est accessible à tous. La partie la plus intéressante de ce montage est la réalisation du coffret qui est solide, esthétique et simple à réaliser. L'inconvénient est qu'il faut avoir des outils que l'on ne trouve pas chez tout le monde: arc à souder et plotter.

![Réalisation][id2]

[id1]: /blog/images/13_copy.jpg "Réalisation"
[id2]: /blog/images/15_copy.jpg "Réalisation"
[id3]: /blog/images/16_copy.jpg "Réalisation"
