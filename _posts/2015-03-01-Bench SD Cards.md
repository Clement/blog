---
# The title of the post.  Optional.  Extracted from the filename if not present.
title: Rasberry Pi 2 et Play Framework

# Any tags for the post. Space separated.  Multi word tags can have their spaces escaped with +
tags: raspberry play

# Optional id. This is used for example when identifying the post to disqus.
# id: some-id
---

Sandisk 2go
Running flashbench
4MiB    6.55M/s 
2MiB    6.11M/s 
1MiB    3.29M/s 
512KiB  2.07M/s 
256KiB  1.16M/s 
128KiB  802K/s  
64KiB   422K/s  
32KiB   221K/s  
16KiB   110K/s  
8KiB    55.8K/s 
4KiB    27.8K/s 

Sandisk extreme 16go
Running flashbench
4MiB    18.1M/s 
2MiB    17.8M/s 
1MiB    11M/s   
512KiB  4.46M/s 
256KiB  1.14M/s 
128KiB  627K/s  
64KiB   348K/s  
32KiB   147K/s  
16KiB   69.2K/s 
8KiB    620K/s  
4KiB    835K/s  

Samsung 8go
Running flashbench
4MiB    4.57M/s 
2MiB    3.96M/s 
1MiB    1.81M/s 
512KiB  875K/s  
256KiB  428K/s  
128KiB  213K/s  
64KiB   107K/s  

San disk ultra 8gb
Running flashbench
4MiB    9.01M/s 
2MiB    8.04M/s 
1MiB    11M/s   
512KiB  3.53M/s 
256KiB  1.12M/s 
128KiB  559K/s  
64KiB   328K/s  
32KiB   151K/s  
16KiB   76K/s   
8KiB    580K/s  
4KiB    803K/s  

San disk USB 3.0 extreme
Running flashbench
4MiB    34.8M/s 
2MiB    33.7M/s 
1MiB    29.8M/s 
512KiB  21.2M/s 
256KiB  18M/s   
128KiB  24.2M/s 
64KiB   15.6M/s 
32KiB   5.93M/s 
16KiB   9.14M/s 
8KiB    5.1M/s  
4KiB    2.64M/s 

Cle USB rose
Running flashbench
4MiB    6.76M/s 
2MiB    5.84M/s 
1MiB    4.31M/s 
512KiB  2.87M/s 
256KiB  1.73M/s 
128KiB  1.02M/s 
64KiB   574K/s  
32KiB   308K/s  
16KiB   155K/s  
8KiB    78.9K/s 
4KiB    40.4K/s 

Cle ROPA
Running flashbench
4MiB    1.94M/s 
2MiB    1.39M/s 
1MiB    1.46M/s 
512KiB  676K/s  
256KiB  344K/s  
128KiB  175K/s  
64KiB   88.9K/s 
