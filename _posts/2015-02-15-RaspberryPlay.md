---
# The title of the post.  Optional.  Extracted from the filename if not present.
title: Rasberry Pi 2 et Play Framework

# Any tags for the post. Space separated.  Multi word tags can have their spaces escaped with +
tags: raspberry play

# Optional id. This is used for example when identifying the post to disqus.
# id: some-id
---

Voici le prémier test de [play Framework] sur un Raspberry 2! Voyons ce que cela donne en utilisant l'outil de benchmark fourni par apache: ab.

```

Benchmarking bricodeur.fr (be patient).....done


Server Software:        
Server Hostname:        bricodeur.fr
Server Port:            80

Document Path:          /blog
Document Length:        4271 bytes

Concurrency Level:      10
Time taken for tests:   3.895 seconds
Complete requests:      100
Failed requests:        0
Total transferred:      437400 bytes
HTML transferred:       427100 bytes
Requests per second:    25.67 [#/sec] (mean)
Time per request:       389.497 [ms] (mean)
Time per request:       38.950 [ms] (mean, across all concurrent requests)
Transfer rate:          109.67 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        2   15  99.7      4    1002
Processing:   187  365  92.8    358     659
Waiting:      186  359  89.4    355     656
Total:        190  380 135.5    365    1354

Percentage of the requests served within a certain time (ms)
  50%    365
  66%    405
  75%    425
  80%    430
  90%    499
  95%    577
  98%    667
  99%   1354
 100%   1354 (longest request)

```
[play Framework]: https://www.playframework.com/
