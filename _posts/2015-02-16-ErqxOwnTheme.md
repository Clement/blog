

---
# The title of the post.  Optional.  Extracted from the filename if not present.
title: ERQX Engine Personnaliser son thème 

# Any tags for the post. Space separated.  Multi word tags can have their spaces escaped with +
tags: erqx play

# Optional id. This is used for example when identifying the post to disqus.
# id: some-id
---

ERQX offre la possibilité de créer un thème personnalisé. Pour cela, il suffit de définir quelles parties de la page le thème personnalisé va prendre en charge puis de définir les templates correspondant.

## Création du thème personnalisé

```
object ThemeBricodeur extends BlogTheme {
  
  val name = "themeBricodeur"
  val hash = {
    val erqxTheme = Hex.decodeHex(DefaultTheme.hash.toCharArray)
    val thisTheme = Hex.decodeHex(Build.hash.toCharArray)
    val hashBytes = erqxTheme.zip(thisTheme).map {
      case (a, b) => (a ^ b).asInstanceOf[Byte]
    }
    Hex.encodeHexString(hashBytes)
  }
  
  override def head(blog: Blog, router: BlogReverseRouter, title: Option[String])(implicit req: play.api.mvc.RequestHeader): Html =
    views.html.head(blog, router, title)
    
  override def blogPost(blog: Blog, router: BlogReverseRouter,
               post: BlogPost, content: String)(implicit req: play.api.mvc.RequestHeader): Html =
    views.html.blogPost(blog, router, post, content)

  override def footer(blog: Blog, router: BlogReverseRouter)(implicit req: play.api.mvc.RequestHeader): Html =
    views.html.footer(blog, router)    
}
```

## Création des templates

Lorsque l'on a crée son thème, on a indiqué quel template le thème doit appeler. Il faut maintenant créer le template qui va retourner le html. On peut soit partir de zéro, mais le plus simple reste de s'inspirer du thème par défaut et de l'adapter à  sa convenance.

## Configuration du blog

Enfin, on paramètre le blog pour qu'il charge le thème que l'on vient de créer. Attention aux messages d'avertissement lors du démarrage car en cas de problème lors du chargement, le thème par défaut sera utilisé.h

```
# The title
title: Bricodeur 

# Optional subtitle
subTitle: Software Craftmanship

# Author - used by atom feed
author: Clément 

# The description - optional, may contain HTML.
description: |
   Just another ERQX Blog 

# The footer - optional, may contain HTML.
footer: |
    Copyright Bricodeur. Tous droits reservés.

# The theme to use, if not specified uses the default theme.
theme: themes.ThemeBricodeur$

# Other arbitrary properties may go here that may be used by the theme.

# The default theme allows specifying a disqus id:
# disqus_shortname: mydisqusblog

```

Si tout s'est bien déroulé, le html généré lors du chargement doit correspondre à  vos modifs. Vérifiez bien le cache de votre navigateur.

[play Framework]: https://www.playframework.com/
